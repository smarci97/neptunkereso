﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NeptunK
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM ViewModel { get; set; }

        public MainWindow()
        {

            InitializeComponent();
            tb_kereso.Focus();

            try
            {
                ViewModel = new VM();
                lb_allapot.Foreground = new SolidColorBrush(Color.FromRgb(38, 115, 38));
            }
            catch (Exception)
            {
                //ViewModel.Allapot = "sikertelen";
                lb_allapot.Content = "Sikertelen";
                lb_allapot.Foreground = new SolidColorBrush(Color.FromRgb(153,0,0));
                tb_kereso.IsReadOnly = true;
            }
            //var context = new dbEntities();
            this.DataContext = ViewModel;
            lb_talalatok.Height = 270;
            this.Height = 430;

        }


        private void tb_kereso_TextChanged(object sender, TextChangedEventArgs e)
        {
            sugo.Visibility = Visibility.Hidden;
            Keres();
            //this.Height = 430;
            //lb_talalatok.Height = 260;
            
            lb_talalatok.ItemsSource = ViewModel.TobbTalalat;
        }
        

        private void Keres()
        {
            ViewModel.Keres(tb_kereso.Text);
            
            lb_talalatok.Visibility = Visibility.Visible;
            if (tb_kereso.Text == "Motyó" || tb_kereso.Text == "Motyo" ||
                tb_kereso.Text == "motyo" || tb_kereso.Text == "motyó")
            {
                this.Height = 650;
                lb_talalatok.Height = 490;
            }
            else
            {
                this.Height = 430;
                lb_talalatok.Height = 270;
            }
        }

        private void lb_talalatok_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel.KivalasztottTalalat != null && ViewModel.KivalasztottTalalat.Neptunkod != null)
            {
                Clipboard.SetText(ViewModel.KivalasztottTalalat.Neptunkod);
                sugo.Content = "Neptunkód kimásolva";
                Stopwatch sw = new Stopwatch();
                sugo.Visibility = Visibility.Visible;
                this.Height = 440;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //ViewModel.UjFelvetele();
        }
        
    }
}
