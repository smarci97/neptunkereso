﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeptunK
{
    [Serializable]
    class VM
    {
        public dbEntities Entities;
        List<Tanulok> OsszTanulo;
        List<Tanulok> tobbTalalat;
        public string allapot;
        private Tanulok kivalasztottTalalat;

        public List<Tanulok> OsszTanulo1 { get => OsszTanulo; set => OsszTanulo = value; }
        public List<Tanulok> TobbTalalat { get => tobbTalalat; set => tobbTalalat = value; }
        public Tanulok KivalasztottTalalat { get => kivalasztottTalalat; set => kivalasztottTalalat = value; }
        public string Allapot { get => allapot; set => allapot = value; }




        public List<Tanulok> TalalatokM { get => talalatokM; set => talalatokM = value; }

        
        public VM()
        {
            OsszTanulo1 = new List<Tanulok>();
            TobbTalalat = new List<Tanulok>();
            KivalasztottTalalat = new Tanulok();


            TalalatokM = new List<Tanulok>();
            //allapot = "";

            InitConnection();
            if (Allapot == "sikertelen")
            {
                //AdatbazisFeltolt();
                //ListahozAd();
                throw new Exception("szar van");
                //alapból bele kell rakni az adatbázisba, nem jo úgy hogy a txt-ből olvasom be
            }
        }

        private void InitConnection()
        {
            Entities = new dbEntities();
            if (Entities != null)
            {
                Allapot = " sikeres";
            }
            else
            {
                Allapot = "sikertelen";
            }
        }

        //private void AdatbazisFeltolt()
        //{
        //    string[] tomb = File.ReadAllLines("../Debug/data.txt", Encoding.Default);
        //    foreach (string item in tomb)
        //    {
        //        string[] tores = item.Split('#');
        //        var newTanulo = new Tanulok()
        //        {
        //            ID = int.Parse(tores[0]),
        //            NEV = tores[2],
        //            NEPTUNKOD = tores[1]
        //        };
        //        Entities.Tanuloks.Add(newTanulo);
        //        Entities.SaveChanges();
        //    }
        //}
        //az adatbázist feltöltöttem, elég csak egyszer

        //public void ListahozAd()
        //{
        //    var query = Entities.Tanuloks.ToList();
        //    foreach (var item in query)
        //    {
        //        OsszTanulo1.Add(item);
        //    }
        //}

        private List<Tanulok> talalatokM;
        bool kocsog = false;
        public void Keres(string szoveg)
        {
            var query = Entities.Tanuloks.Where(x => x.Nev.Contains(szoveg) || x.Neptunkod.Contains(szoveg)).ToList();

            if (kocsog == false)
            {   //egy üres listába kerüljenek bele a találatok
                tobbTalalat.Clear();
                TobbTalalat = query.ToList();
            }
            else
            {
                //mtoyo 
                foreach (var item in query)
                {
                    TobbTalalat.Add(item);
                }
            }

            if (szoveg.ToLower() == "motyó" || szoveg.ToLower() == "motyo")
            {
                kocsog = true;
                Keres("Prohászka Ádám");
                Keres("Szilágyi Ádám");
                Keres("Balogh Árpád");
                Keres("Szvoboda Bálint");
                Keres("Bozár Bence");
                Keres("Csillag Bence");
                Keres("Gergely Bálint");
                Keres("Krujz Gergely");
                Keres("Nagy Gergő");
                Keres("HL6ZTK");//Kiss Ádám :D 
                Keres("Farkas Loránd");
                Keres("Sándorfi Marcell");
                Keres("Végh Norbert");
                Keres("Röhberg Péter");
                Keres("Ugróczky Zsolt");
                kocsog = false;
            }
        }
        
        public void UjFelvetele()
        {
            var newStudent = new Tanulok
            {
                Nev = "Csonka Barbara",
                Neptunkod = "AHGKQD",
                ID = 459
            };
            Entities.Tanuloks.Add(newStudent);
            Entities.SaveChanges();
            
        }
    }
    

    //public void Kereso(string szoveg)
    //{
    //    talalatok = 0;
    //    if (kocsog == false)
    //    {
    //        TobbTalalat.Clear();
    //    }

    //    foreach (var item in OsszTanulo1)
    //    {
    //        if ((item.Nev.ToLower().Contains(szoveg.ToLower()) || item.Neptunkod.ToLower().Contains(szoveg.ToLower())))
    //        {
    //            bool bennevae = false;
    //            foreach (var t in TobbTalalat)
    //            {
    //                if (t.Neptunkod == item.Neptunkod)
    //                {//ha tartalmazza (már szerepel a találatok közt), ne tegyük bele
    //                    bennevae = true;
    //                }
    //            }
    //            if (bennevae == false)
    //            {
    //                talalatok++;
    //                TobbTalalat.Add(item);
    //            }
    //        }
    //    }

    //    if (szoveg == "Motyó" || szoveg == "Motyo" || szoveg == "motyo" || szoveg == "motyó")
    //    {
    //        kocsog = true;
    //        Keres("Prohászka Ádám");
    //        Keres("Szilágyi Ádám");
    //        Keres("Balogh Árpád");
    //        Keres("Szvoboda Bálint");
    //        Keres("Bozár Bence");
    //        Keres("Csillag Bence");
    //        Keres("Gergely Bálint");
    //        Keres("Krujz Gergely");
    //        Keres("Nagy Gergő");
    //        Keres("HL6ZTK");//Kiss Ádám :D 
    //        Keres("Farkas Loránd");
    //        Keres("Sándorfi Marcell");
    //        Keres("Végh Norbert");
    //        Keres("Röhberg Péter");
    //        Keres("Ugróczky Zsolt");
    //        kocsog = false;
    //    }
    //}

    //}
}
